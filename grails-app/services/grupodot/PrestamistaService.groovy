package grupodot

import grails.gorm.services.Service

@Service(Prestamista)
interface PrestamistaService {

    Prestamista get(Serializable id)

    List<Prestamista> list(Map args)

    Long count()

    void delete(Serializable id)

    Prestamista save(Prestamista prestamista)

}