<%--
  Created by IntelliJ IDEA.
  User: 1045721591
  Date: 7/09/2018
  Time: 11:04 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Solicitar Prestamo</title>
</head>

<body>
    <g:form name="solicitoPrestamo" controller="prestamo" action="calcularCotizacion">
        <g:message default="Monto préstamo" message="Monto préstamo"/>
        <g:textField name="montoPrestamo" value="${params.montoPrestamo}" />
        <g:submitButton name="Calcular cotización" />
    </g:form>

    <g:message default="Monto préstamo" message="Valor a Pagar"/>
    <g:textField name="ajaxCall" value="${valorAPagar}" disabled="true"/>
    <br><br>
    <table border="2">
        <tr>
            <td><b>Socio que realiza el préstamo</b></td>
            <td><b>Cuota Mensual</b></td>
            <td><b>Pago total del crédito</b></td>
            <td><b>Tasa de interés mensual</b></td>
        </tr>
        <tr>
            <td>${usuarioPrestamista?.nombre?:''}</td>
            <td>$ ${cuotaMensual}</td>
            <td>$ ${valorAPagar}</td>
            <td>${usuarioPrestamista?.tasa?:''}%</td>
        </tr>

    </table>

<g:if test="${!usuarioPrestamista}">
    <h1>${msj}</h1>
</g:if>




</body>
</html>