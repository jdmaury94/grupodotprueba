package grupodot

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class PrestamistaController {

    PrestamistaService prestamistaService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond prestamistaService.list(params), model:[prestamistaCount: prestamistaService.count()]
    }

    def show(Long id) {
        respond prestamistaService.get(id)
    }

    def create() {
        respond new Prestamista(params)
    }

    def save(Prestamista prestamista) {
        if (prestamista == null) {
            notFound()
            return
        }

        try {
            prestamistaService.save(prestamista)
        } catch (ValidationException e) {
            respond prestamista.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'prestamista.label', default: 'Prestamista'), prestamista.id])
                redirect prestamista
            }
            '*' { respond prestamista, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond prestamistaService.get(id)
    }

    def update(Prestamista prestamista) {
        if (prestamista == null) {
            notFound()
            return
        }

        try {
            prestamistaService.save(prestamista)
        } catch (ValidationException e) {
            respond prestamista.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'prestamista.label', default: 'Prestamista'), prestamista.id])
                redirect prestamista
            }
            '*'{ respond prestamista, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        prestamistaService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'prestamista.label', default: 'Prestamista'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'prestamista.label', default: 'Prestamista'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
