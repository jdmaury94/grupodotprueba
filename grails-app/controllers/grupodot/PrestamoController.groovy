package grupodot

import grails.validation.ValidationException

import java.text.DecimalFormat

import static org.springframework.http.HttpStatus.*

class PrestamoController {

    PrestamoService prestamoService

    static allowedMethods = [save: "POST", show: "GET"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond prestamoService.list(params), model:[prestamoCount: prestamoService.count()]
    }

    def show(Long id) {
        respond prestamoService.get(id)
    }

    def create() {
        respond new Prestamo(params)
    }

    def save(Prestamo prestamo) {
        if (prestamo == null) {
            notFound()
            return
        }

        try {
            prestamoService.save(prestamo)
        } catch (ValidationException e) {
            respond prestamo.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'prestamo.label', default: 'Prestamo'), prestamo.id])
                redirect prestamo
            }
            '*' { respond prestamo, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond prestamoService.get(id)
    }

    def update(Prestamo prestamo) {
        if (prestamo == null) {
            notFound()
            return
        }

        try {
            prestamoService.save(prestamo)
        } catch (ValidationException e) {
            respond prestamo.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'prestamo.label', default: 'Prestamo'), prestamo.id])
                redirect prestamo
            }
            '*'{ respond prestamo, [status: OK] }
        }
    }


    def calcularCotizacion(){


        String msj=""
        def usuarioPrestamista
        double cuotaMensual
        double valorAPagar
        DecimalFormat df2 = new DecimalFormat("##.00");

        try {
            if(params.montoPrestamo && request.method=='POST'){
                int montoPrestamo=Integer.parseInt(params.montoPrestamo)
                def usuarioPrestamistaQuery="From Prestamista p where p.montoMaximo>${montoPrestamo} Order By p.tasa"
                usuarioPrestamista=Prestamista.executeQuery(usuarioPrestamistaQuery)[0]

                if(usuarioPrestamista)
                {
                    double tasaUsuarioPrestamista=usuarioPrestamista.tasa/100
                    int plazoFijo=36
                    valorAPagar=montoPrestamo*(1+plazoFijo*tasaUsuarioPrestamista)
                    cuotaMensual=valorAPagar/plazoFijo

                    //println request.JSON["name"]
                }
                else
                    msj="No hay gente que cumpla  con los requisitos"
            }
            else
            {
                if(request.method!='POST')
                {
                    response.status=500
                    msj="Método no autorizado"
                }
                else
                    msj="No se ha ingresado información"

            }

        }catch(Exception ex){
            msj="Verifique la información!"
        }



        render view: "solicitarPrestamo",model: [valorAPagar:df2.format(valorAPagar)?:'',usuarioPrestamista:usuarioPrestamista,
                                                 cuotaMensual:df2.format(cuotaMensual)?:'',msj:msj]

    }






    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        prestamoService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'prestamo.label', default: 'Prestamo'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'prestamo.label', default: 'Prestamo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
