package grupodot

class BootStrap {

    def init = { servletContext ->

        //new Helado(sabor: "The Stand").save()
        //new Helado(sabor:"The Shining").save()
        new Prestamista(nombre: "Juan",tasa: 1.5,montoMaximo: 5000000).save()
        new Prestamista(nombre: "Andrés",tasa: 2.0,montoMaximo: 7500000).save()
        new Prestamista(nombre: "María",tasa: 1.2,montoMaximo: 3000000).save()


    }
    def destroy = {
    }
}
