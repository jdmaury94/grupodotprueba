package grupodot

class Prestamo {

    int montoPrestamo
    int cuotaMensual
    int pagoTotalCredito
    float tasaInteresMensual

    static constraints = {
    }

    static belongsTo = [
            prestamista:Prestamista
    ]

    static mapping = {
        version false
    }
}
