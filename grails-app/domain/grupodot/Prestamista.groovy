package grupodot

class Prestamista {

    String nombre
    float tasa
    int montoMaximo

    static constraints = {
    }

    static  hasMany = [
            prestamo:Prestamo
    ]

    static mapping = {
        version false
    }
}
