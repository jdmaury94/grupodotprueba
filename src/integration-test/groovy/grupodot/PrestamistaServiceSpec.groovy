package grupodot

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class PrestamistaServiceSpec extends Specification {

    PrestamistaService prestamistaService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Prestamista(...).save(flush: true, failOnError: true)
        //new Prestamista(...).save(flush: true, failOnError: true)
        //Prestamista prestamista = new Prestamista(...).save(flush: true, failOnError: true)
        //new Prestamista(...).save(flush: true, failOnError: true)
        //new Prestamista(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //prestamista.id
    }

    void "test get"() {
        setupData()

        expect:
        prestamistaService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Prestamista> prestamistaList = prestamistaService.list(max: 2, offset: 2)

        then:
        prestamistaList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        prestamistaService.count() == 5
    }

    void "test delete"() {
        Long prestamistaId = setupData()

        expect:
        prestamistaService.count() == 5

        when:
        prestamistaService.delete(prestamistaId)
        sessionFactory.currentSession.flush()

        then:
        prestamistaService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Prestamista prestamista = new Prestamista()
        prestamistaService.save(prestamista)

        then:
        prestamista.id != null
    }
}
